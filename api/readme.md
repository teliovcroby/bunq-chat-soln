# Description

This repo contains an implementation of a simple chat REST API which performs the following:
 - Allows for registration and authentication of users 
 - Allows users to send messages to each other
 - Allows polling for new messages
 
 # Setup
The quickest (and tested) way to get up and running is via `docker-compose`

Run the following commands:
- `cp provision/api.env .env`
- `docker-compose up -d`
- `docker-compose exec api composer install`

If the `EXPOSE_API_PORT` default variable has not been changed, you can access the API at `http://localhost:9000`

# Documentation

A postman collection is included in the `docs` folder.

Also a html version of the postman collection (generated using [postmanerator](https://github.com/aubm/postmanerator)) is included (`docs/index.html`)

# Running Tests

Included unit tests can be run using:
```shell script
docker-compose exec api composer run test 
```