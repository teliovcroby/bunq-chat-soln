<?php

$container = require_once dirname(dirname(__FILE__))."/bootstrap.php";

use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response;
use League\Route\RouteCollection;

$router = $container->make(RouteCollection::class);

$request = ServerRequestFactory::fromGlobals();

$response = new Response();
$emitter = new SapiEmitter();

$response = $router->dispatch($request, $response);

$emitter->emit($response);
