<?php


namespace App\Test\Services;


use App\Models\Pagination\MyLengthAwarePaginator;
use App\Services\UserManager;
use App\Test\Base;

class UserManagerTest extends Base
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $users = [
            [
                'username' => 'teliov4',
                'password' => 'test123'
            ],
            [
                'username' => 'teliov1234',
                'password' => 'test123'
            ],
            [
                'username' => 'james',
                'password' => 'test123'
            ]
        ];

        $pdo = self::getConnection();
        $sql = "insert into users (username, password) values (?, ?)";
        $stmt = $pdo->prepare($sql);

        foreach ($users as $user)
        {
            $stmt->execute([$user['username'], $user['password']]);
        }
    }

    public function testListUsers()
    {
        $manager = new UserManager();
        $users = $manager->listUsers();

        $this->assertInstanceOf(MyLengthAwarePaginator::class, $users);

        $arr = $users->toArray();
        $this->assertEquals(3, count($arr['data']));

    }

    public function testListUsersWithUserName()
    {
        $manager = new UserManager();
        $users = $manager->listUsers(100, 1, 'tel');

        $this->assertInstanceOf(MyLengthAwarePaginator::class, $users);
        $arr = $users->toArray();
        $this->assertEquals(2, count($arr['data']));
    }
}