<?php


namespace App\Test\Services;


use App\Models\Mysql\AccessToken;
use App\Services\AuthService;
use App\Test\Base;

class AuthServiceTest extends Base
{
    public function getService()
    {
        return new AuthService();
    }
    public function testHandleRegistration()
    {
        $username = 'newuser1';
        $password = 'somepassword';
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        $user = new \StdClass;
        $user->id = 1;

        $authService = \Mockery::mock(AuthService::class)->makePartial();
        $authService->shouldReceive('userExistsByUsername')
            ->andReturn(false);

        $authService->shouldReceive('createUser')
            ->withArgs(function ($uname, $phash) use ($username) {
                if ($uname == $username) return true;
                return false;
            })
            ->andReturn($user);

        $res = $authService->handleRegistration($username, $password);

        $this->assertArrayHasKey('user', $res);
        $this->assertArrayHasKey('token_data', $res);
    }

    public function testAuthenticateUser()
    {
        $username = 'agooduser';
        $password = 'somepassword';
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        $user = new \StdClass;
        $user->id = 1;
        $user->username = $username;
        $user->password = $passwordHash;

        $authService = \Mockery::mock(AuthService::class)->makePartial();
        $authService->shouldReceive('getUserByUsername')
            ->withArgs([$username])
            ->andReturn($user);

        $res = $authService->authenticateUser($username, $password);

        $this->assertArrayHasKey('user', $res);
        $this->assertArrayHasKey('token_data', $res);

        $tokenData = $res['token_data'];
        $this->assertArrayHasKey('token', $tokenData);
        $this->assertArrayHasKey('expires_at', $tokenData);

        return $tokenData['token'];
    }

    /**
     * @depends testAuthenticateUser
     */
    public function testParseToken($tokenString)
    {
        $authService = \Mockery::mock(AuthService::class)->makePartial();
        $authService->shouldReceive('getUserAccessToken')
            ->withArgs(function ($tokenId, $userId) {
                if ($userId == 1) return true;
                return false;
            })
            ->andReturn(true);

        $authService->shouldReceive('getUserById')
            ->withArgs([1])
            ->andReturn(new \StdClass);

        $res = $authService->parseToken($tokenString);
        $this->assertNotNull($res);
    }

    /**
     * @depends testAuthenticateUser
     */
    public function testRevokeToken($tokenString)
    {
        $accessToken = \Mockery::mock();
        $accessToken
            ->shouldReceive('delete')
            ->andReturn(true);

        $authService = \Mockery::mock(AuthService::class)->makePartial();
        $authService->shouldReceive('getUserAccessToken')
            ->andReturn($accessToken);

        $res = $authService->revokeToken($tokenString);
        $this->assertTrue($res);
    }
}