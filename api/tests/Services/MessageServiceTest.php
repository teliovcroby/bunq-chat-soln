<?php


namespace App\Test\Services;


use App\Models\Pagination\MyLengthAwarePaginator;
use App\Services\MessageService;
use App\Test\Base;

class MessageServiceTest extends Base
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $users = [
            [
                'username' => 'teliov4',
                'password' => 'test123'
            ],
            [
                'username' => 'teliov1234',
                'password' => 'test123'
            ],
            [
                'username' => 'james',
                'password' => 'test123'
            ]
        ];

        $pdo = self::getConnection();
        $sql = "insert into users (username, password) values (?, ?)";
        $stmt = $pdo->prepare($sql);

        foreach ($users as $user)
        {
            $stmt->execute([$user['username'], $user['password']]);
        }

        $messages = [
            [
                'sender_id' => 1,
                'recipient_id' => 2,
                'message' => 'hello',
                'created_at' => '2020-06-04 07:35:58'
            ],
            [
                'sender_id' => 1,
                'recipient_id' => 2,
                'message' => 'hello',
                'created_at' => '2020-06-04 08:35:58'
            ],
            [
                'sender_id' => 1,
                'recipient_id' => 3,
                'message' => 'hello',
                'created_at' => '2020-06-04 09:35:58'
            ],
            [
                'sender_id' => 1,
                'recipient_id' => 3,
                'message' => 'hello',
                'created_at' => '2020-06-04 17:35:58'
            ],
            [
                'sender_id' => 1,
                'recipient_id' => 2,
                'message' => 'hello',
                'created_at' => '2020-06-04 18:35:58'
            ]
        ];

        $sql = "insert into messages (message, sender_id, receiver_id, created_at) values (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);

        foreach ($messages as $message)
        {
            $stmt->execute([$message['message'], $message['sender_id'], $message['recipient_id'], $message['created_at']]);
        }
    }

    public function testPostMessage()
    {
        $sender = new \StdClass;
        $sender->id = 1;
        $recipientId = 2;
        $text = "hello";
        $messageService = \Mockery::mock(MessageService::class)->makePartial();

        $messageService->shouldReceive('userExistsById')
            ->withArgs([$recipientId])
            ->andReturn(true);

        $messageService->shouldReceive('saveUserMessage')
            ->withArgs([$sender->id, $recipientId, $text])
            ->andReturn(new \StdClass);

        $message = $messageService->postMessage($sender, $recipientId, $text);
        $this->assertNotNull($message);
    }

    public function testListUserMessages()
    {
        $messageService = new MessageService();
        $messages = $messageService->listUserMessages(1);

        $this->assertInstanceOf(MyLengthAwarePaginator::class, $messages);

        $arr = $messages->toArray();
        $this->assertEquals(5, count($arr['data']));
    }

    public function testFilterMessagesByUsername()
    {
        $messageService = new MessageService();
        $q = [
            'username' => 'teliov1234'
        ];
        $messages = $messageService->listUserMessages(1, $q);
        $this->assertInstanceOf(MyLengthAwarePaginator::class, $messages);

        $arr = $messages->toArray();
        $this->assertEquals(3, count($arr['data']));
    }

    public function testFilterMessagesByAfter()
    {
        $messageService = new MessageService();
        $q = [
            'after' => '2020-06-04 08:35:59'
        ];
        $messages = $messageService->listUserMessages(1, $q);
        $this->assertInstanceOf(MyLengthAwarePaginator::class, $messages);

        $arr = $messages->toArray();
        $this->assertEquals(3, count($arr['data']));
    }

    public function testFilterMessagesByBefore()
    {
        $messageService = new MessageService();
        $q = [
            'before' => '2020-06-04 08:35:58'
        ];
        $messages = $messageService->listUserMessages(1, $q);
        $this->assertInstanceOf(MyLengthAwarePaginator::class, $messages);

        $arr = $messages->toArray();
        $this->assertEquals(2, count($arr['data']));
    }

    public function testFilterMessagesByAfterAndBefore()
    {
        $messageService = new MessageService();
        $q = [
            'before' => '2020-06-04 17:35:58',
            'after' => '2020-06-04 08:35:59'
        ];
        $messages = $messageService->listUserMessages(1, $q);
        $this->assertInstanceOf(MyLengthAwarePaginator::class, $messages);

        $arr = $messages->toArray();
        $this->assertEquals(2, count($arr['data']));
    }

    public function testListUserChats()
    {
        $messageService = new MessageService();
        $messages = $messageService->listUserChats(1);
        $this->assertEquals(2, count($messages));

        $messages = $messageService->listUserChats(2);
        $this->assertEquals(1, count($messages));

        $messages = $messageService->listUserChats(3);
        $this->assertEquals(1, count($messages));
    }
}