<?php

namespace App\Test;

use App\Test\Utils\DbHelpers;
use League\FactoryMuffin\FactoryMuffin;
use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class Base extends TestCase
{
    use DbHelpers;

    use MockeryPHPUnitIntegration;
    /**
     * perform actions that need to be carried out before each test set,
     * e.g. resetting a db, etc
     */
    public static function setUpBeforeClass() : void
    {
        parent::setUpBeforeClass();

        static::setupDB();
    }

    /**
     * perform actions that need to be setup before each test case e.g loading factories
     */
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function validateObjectKeys($object, $keys)
    {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $object);
        }
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        static::tearDownDB();
    }

}
