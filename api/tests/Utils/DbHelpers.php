<?php

namespace App\Test\Utils;

use Illuminate\Database\Capsule\Manager as DBManager;
use League\FactoryMuffin\FactoryMuffin;
use Phinx\Config\Config;
use Phinx\Migration\Manager;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

trait DbHelpers
{

    public static $pdo;

    public static function getConnection()
    {
        if (!self::$pdo) {
            $dsn = "sqlite::memory:";
            self::$pdo  = new \PDO($dsn, null, null, [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ]);
        }

        return self::$pdo;
    }

    public static function setupDB()
    {
        $pdo = self::getConnection();

        $configFilePath = app_base_dir()."/phinx.php";
        $configArray = require $configFilePath;

        $configArray['environments']['test']['connection'] = $pdo;

        $config = new Config($configArray, $configFilePath);
        $manager = new Manager($config, new StringInput(' '),  new NullOutput());
        $manager->migrate('test');

        /**
         * @var DBManager
         */
        $dbManager = app()->make(DBManager::class);

        $dbManager->addConnection([
            'driver' => 'sqlite',
            'database' => ':memory:',
        ], 'sqlite');
        $dbManager->getConnection('sqlite')->setPdo($pdo);
        $dbManager->getDatabaseManager()->setDefaultConnection('sqlite');
        $dbManager->bootEloquent();
    }

    public static function tearDownDB()
    {
        $sqls =
        [
            "drop table messages",
            "drop table access_tokens",
            "drop table users"
        ];

        $pdo = self::getConnection();
        foreach ($sqls as $sql) {
            $pdo->exec($sql);
        }

        self::$pdo = null;
    }
}