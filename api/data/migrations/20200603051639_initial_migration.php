<?php


use Phinx\Migration\AbstractMigration;

class InitialMigration extends AbstractMigration
{
    public function up()
    {
        $this->table('users')
            ->addColumn('username', 'string')
            ->addColumn('password', 'string')
            ->addIndex('username', ['unique' => true])
            ->addTimestamps()
            ->create();

        $this->table('access_tokens')
            ->addColumn('token', 'string')
            ->addColumn('user_id', 'integer')
            ->addColumn('expires_at', 'timestamp')
            ->addForeignKey('user_id', 'users', 'id')
            ->addTimestamps()
            ->create();

        $this->table('messages')
            ->addColumn('message', 'text')
            ->addColumn('sender_id', 'integer')
            ->addColumn('receiver_id', 'integer')
            ->addForeignKey('sender_id', 'users', 'id')
            ->addForeignKey('receiver_id', 'user', 'id')
            ->addTimestamps()
            ->create();
    }

    public function down()
    {
        $this->table('messages')->drop();
        $this->table('access_tokens')->drop();
        $this->table('users')->drop();
    }
}
