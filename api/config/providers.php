<?php

use App\Providers\DatabaseProvider;
use App\Providers\EventServiceProvider;
use App\Providers\RouterServiceProvider;
use App\Providers\PolicyManagerProvider;

return [
    EventServiceProvider::class, # should always be first
    DatabaseProvider::class,
    RouterServiceProvider::class,
    PolicyManagerProvider::class
];