<?php

use App\Models\Mysql\Policies\UserPolicy;
use App\Models\Mysql\User;

$policies = [
    User::class => UserPolicy::class,
];

return $policies;
