<?php

/**
 * Route definitions for the project go here
 */
use App\Middleware\AuthMiddleware;
use App\Middleware\BasicAuthMiddleware;
use App\RouterStrategies\ApiStrategy;
use League\Route\RouteCollection;
use League\Route\RouteGroup;

$arr = compact("container");

if (!isset($arr["container"])) {
    throw new Exception("No container instance available");
}

$container = $arr['container'];

$router = new RouteCollection($container);
$router->setStrategy(new ApiStrategy($container));

$authMiddleware = new AuthMiddleware($container);
$basicAuthMiddleware = new BasicAuthMiddleware($container);

# sample response to get you going
$router->get('/', function () {
    $response = new \Laminas\Diactoros\Response();
    $response->getBody()->write(json_encode([
        "application" => "Bunq Chat-Zone",
        "status" => true,
        "version" => "1.0.0"
    ]));
    $response = $response->withStatus(200)->withHeader('Content-Type', 'application/json');
    return $response;
});

$router->group("/auth", function (RouteGroup $group) use($basicAuthMiddleware, $authMiddleware) {

    $group->get('/whoami', '\App\Controllers\AuthController::whoAmI')
        ->middleware($authMiddleware);

    $group->post('/user/token', '\App\Controllers\AuthController::getAccessTokenForUser')
        ->middleware($basicAuthMiddleware);

    $group->post('/token/revoke', '\App\Controllers\AuthController::revokeAccessToken')
        ->middleware($authMiddleware);

    $group->post('/user/registration', '\App\Controllers\AuthController::handleRegistration')
        ->middleware($basicAuthMiddleware);
});


$router->group("/users", function (RouteGroup $group) use ($authMiddleware) {
    $group->get('/', '\App\Controllers\UserController::listUsers')
        ->middleware($authMiddleware);

    $group->get('/{userId: \d+}', '\App\Controllers\UserController::getUserProfile')
        ->middleware($authMiddleware);

    $group->get('/{userId: \d+}/messages', '\App\Controllers\UserController::listUserMessages')
        ->middleware($authMiddleware);

    $group->get('/{userId: \d+}/chats', '\App\Controllers\UserController::listUserChats')
        ->middleware($authMiddleware);
});

$router->group("/messages", function (RouteGroup $group) use ($authMiddleware) {
    $group->post('/', '\App\Controllers\MessageController::postChatMessage')
        ->middleware($authMiddleware);
});

$router->middleware(new \App\Middleware\Cors\CorsMiddleware($container));

return $router;
