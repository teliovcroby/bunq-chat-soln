<?php

require_once dirname(__FILE__)."/vendor/autoload.php";

use App\AppContainer;
use Dotenv\Dotenv;
use Illuminate\Container\Container;
use Interop\Container\ContainerInterface;
use Illuminate\Contracts\Container\Container as ContainerContract;

$dotenv = new Dotenv(dirname(__FILE__));
$dotenv->load();

$container = new AppContainer();

Container::setInstance($container);

# some container bindings so the container knows to inject itself
$container->instance(Container::class, $container);
$container->alias(Container::class, AppContainer::class);
$container->bind(ContainerContract::class, Container::class);
$container->bind(ContainerInterface::class, Container::class);

$baseDir = dirname(__FILE__);
if (!function_exists("app_base_dir")) {
    /**
     * @return string
     */
    function app_base_dir()
    {
        return dirname(__FILE__);
    }
}

if (!function_exists("app_config_dir")) {
    /**
     * @return string
     */
    function app_config_dir()
    {
        return dirname(__FILE__)."/config";
    }
}

if (!function_exists("app")) {
    /**
     * @return Container
     */
    function app()
    {
        return Container::getInstance();
    }
}

// Run through the service providers and register them

$providers = require_once $baseDir."/config/providers.php";
foreach ($providers as $provider) {
    (new $provider)->setContainer($container)->register();
}

return $container;
