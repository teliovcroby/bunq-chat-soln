<?php
/**
 * Created by PhpStorm.
 * User: teliov
 * Date: 10/12/17
 * Time: 12:16 PM
 */

namespace App\Access\Policies;

abstract class AbstractPolicy
{
    public abstract function create($resourceObj, $actorObj);

    public abstract function view($resourceObj, $actorObj);

    public abstract function update($resourceObj, $actorObj);

    public abstract function delete($resourceObj, $actorObj);
}
