<?php
/**
 * Created by PhpStorm.
 * User: teliov
 * Date: 10/12/17
 * Time: 12:15 PM
 */

namespace App\Access\Policies;

class BasePolicy extends AbstractPolicy
{
    public function create($resourceObj, $actorObj)
    {
        return false;
    }

    public function update($resourceObj, $actorObj)
    {
        return false;
    }

    public function view($resourceObj, $actorObj)
    {
        return false;
    }
    public function delete($resourceObj, $actorObj)
    {
        return false;
    }
}
