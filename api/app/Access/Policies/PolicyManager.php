<?php

namespace App\Access\Policies;

use App\Contracts\ContainerContract;
use App\Exceptions\PermissionException;
use Illuminate\Contracts\Container\Container;

class PolicyManager
{
    use ContainerContract;

    protected $policies = [];

    protected $executioners = [];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function registerPolicy($resource, $policy, $executioner)
    {
        if (!is_string($resource)) {
            throw new PolicyException('Only string representation of resource class is allowed');
        }

        if (!in_array($policy, $this->allowedResourcePolicies())) {
            $message = sprintf("Specified Policy Not allowed: %s", $policy);
            throw new PolicyException($message);
        }

        $policyUid = $resource.'.'.$policy;

        if (is_object($executioner)) {
            if ($executioner instanceof \Closure) {
                $this->executioners[$policyUid] = $executioner;
            } else {
                if (!method_exists($executioner, $policy)) {
                    $message = sprintf("Provided executioner object does not implement the %s method", $policy);
                    throw new PolicyException($message);
                }

                $this->executioners[$policyUid] = [$executioner, $policy];
            }
        }

        // has to be in the Class::method format
        if (is_string($executioner)) {
            $parts = explode('::', $executioner);
            $className = $parts[0] ?? null;
            $method = $parts[1] ?? null;

            if (is_null($className) || is_null($method)) {
                $message = sprintf('Improperly defined policy executioner: %s', $executioner);
                throw new PolicyException($message);
            }

            $this->policies[$policyUid] = [$className, $method];
        }
    }

    public function registerPolicyForResource($resource, $policyExecutioner)
    {
        $policies = $this->allowedResourcePolicies();

        foreach ($policies as $policy) {
            $this->registerPolicy($resource, $policy, $policyExecutioner."::".$policy);
        }
    }

    public function allowedResourcePolicies()
    {
        return [
            'create', 'update', 'delete', 'view'
        ];
    }

    public function executePolicy($policyUid, $resourceObject, $actorObject)
    {
        if (is_array($policyUid)) {
            $policyUid = implode(".", $policyUid);
        }

        if (isset($this->executioners[$policyUid])) {
            $callable = $this->executioners[$policyUid];
            $isOk = call_user_func_array($callable, [$resourceObject, $actorObject]);
        } elseif (isset($this->policies[$policyUid])) {
            $executioner = $this->policies[$policyUid];
            $className = $executioner[0];
            $method = $executioner[1];
            $obj = $this->container->make($className);
            $this->executioners[$policyUid] = [$obj, $method];
            unset($this->policies[$policyUid]);
            $isOk = call_user_func_array([$obj, $method], [$resourceObject, $actorObject]);
        } else {
            $message = sprintf("No Executioner found for policy: %s", $policyUid);
            throw new PolicyException($message);
        }

        if (!$isOk) {
            throw new PermissionException();
        }

        return $isOk;
    }

    public function hasPolicyExecutioner($policyUid)
    {
        return isset($this->executioners[$policyUid]) || isset($this->policies[$policyUid]);
    }
}
