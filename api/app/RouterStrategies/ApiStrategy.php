<?php

namespace App\RouterStrategies;

use App\Contracts\ContainerContract;
use App\Exceptions\MethodNotAllowedException as NotAllowed;
use App\Exceptions\BaseException;
use App\Exceptions\ServerException;
use App\Exceptions\RouteNotFoundException;
use App\Middleware\Cors\CorsMiddleware;
use Exception;
use Illuminate\Contracts\Container\Container;
use League\Route\Http\Exception as HttpException;
use League\Route\Http\Exception\MethodNotAllowedException;
use League\Route\Http\Exception\NotFoundException;
use League\Route\Route;
use League\Route\Strategy\StrategyInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ApiStrategy implements StrategyInterface
{
    use ContainerContract;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getCallable(Route $route, array $vars)
    {
        return function (
            ServerRequestInterface $request,
            ResponseInterface $response,
            callable $next
        ) use (
            $route,
            $vars
        ) {
            $vars["response"] = $response;
            $vars["request"] = $request;
            $return = $this->container->call($route->getCallable(), $vars);

            if (!$return instanceof ResponseInterface) {
                throw new \RuntimeException(
                    'Route callables must return an instance of (Psr\Http\Message\ResponseInterface)'
                );
            }

            $response = $return;
            $response = $next($request, $response);
            return $response;
        };
    }

    public function getNotFoundDecorator(NotFoundException $exception)
    {
        $container = $this->container;
        return function (ServerRequestInterface $request, ResponseInterface $response) use ($container) {
            $nienaException = new RouteNotFoundException();
            $response->getBody()->write(json_encode($nienaException->toArray()));
            $cors = new CorsMiddleware($container);
            $response = $cors($request, $response, function ($_, $res) {
                return $res;
            });
            return $response->withAddedHeader('content-type', 'application/json')->withStatus(404);
        };
    }

    public function getMethodNotAllowedDecorator(MethodNotAllowedException $exception)
    {
        $container = $this->container;
        return function (ServerRequestInterface $request, ResponseInterface $response) use ($container) {
            $nienaException = new NotAllowed();
            $response->getBody()->write(json_encode($nienaException->toArray()));
            $cors = new CorsMiddleware($container);
            $response = $cors($request, $response, function ($req, $res) {
                return $res;
            });
            return $response->withAddedHeader('content-type', 'application/json')->withStatus(405);
        };
    }

    public function getExceptionDecorator(Exception $exception)
    {
        $container = $this->container;
        return function (ServerRequestInterface $request, ResponseInterface $response) use ($container, $exception) {
            if ($exception instanceof HttpException) {
                $response = $exception->buildJsonResponse($response);
            } elseif ($exception instanceof BaseException) {
                $body = $exception->toArray();
                $response->getBody()->write(json_encode($body));
                $response = $response->withStatus($body["http_status_code"])->withAddedHeader('content-type', 'application/json');
            } else {
                $devEnvironment = getenv("ENV");
                if ($devEnvironment === "development") {
                    $e = new ServerException("", 0, $exception);
                    $body = $e->toArray();
                } else {
                    $e = new ServerException("Something went wrong. We're working to fix this");
                    $body = $e->toArray();
                }
                $response->getBody()->write(json_encode($body));
                $response = $response->withStatus(500)->withAddedHeader('content-type', 'application/json');
            }

            $cors = new CorsMiddleware($container);
            $response = $cors($request, $response, function ($_, $res) {
                return $res;
            });
            return $response;
        };
    }
}
