<?php


namespace App\Middleware;


use App\Exceptions\AuthenticationException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class BasicAuthMiddleware extends AbstractMiddleware
{

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $header = $request->getHeader('Authorization')[0] ?? null;

        if (!$header) {
            throw new AuthenticationException();
        }

        $parts = explode(" ", $header);
        if (count($parts) != 2) {
            throw new AuthenticationException();
        }

        if ($parts[0] != "Basic") {
            throw new AuthenticationException();
        }

        $authCombo = base64_decode($parts[1]);
        $split = explode(":", $authCombo);
        if (count($split) <= 1) {
            throw new AuthenticationException();
        }

        $clientId = $split[0];
        $clientSecret = implode(":", array_slice($split, 1));

        if (!hash_equals(getenv('APP_CLIENT_ID'), $clientId)) {
            throw new AuthenticationException();
        }

        if (!hash_equals(getenv('APP_CLIENT_SECRET'), $clientSecret)) {
            throw new AuthenticationException();
        }

        return $next($request, $response);
    }
}