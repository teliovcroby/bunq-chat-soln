<?php

namespace App\Middleware\Cors;

use App\Middleware\AbstractMiddleware;
use League\Route\Http\Exception\BadRequestException;
use Neomerx\Cors\Analyzer;
use Neomerx\Cors\Contracts\AnalysisResultInterface;
use Neomerx\Cors\Contracts\Constants\CorsResponseHeaders;
use Neomerx\Cors\Contracts\Strategies\SettingsStrategyInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CorsMiddleware extends AbstractMiddleware
{
    /**
     * @var SettingsStrategyInterface
     */
    protected $settings;

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $analyzer = Analyzer::instance($this->getSettings());

        $cors = $analyzer->analyze($request);

        switch ($cors->getRequestType()) {
            case AnalysisResultInterface::ERR_ORIGIN_NOT_ALLOWED:
                $message = "CORS request origin is not allowed.";
                break;
            case AnalysisResultInterface::ERR_METHOD_NOT_SUPPORTED:
                $message = "CORS requested method is not supported.";
                break;
            case AnalysisResultInterface::ERR_HEADERS_NOT_SUPPORTED:
                $message = "CORS requested header is not allowed.";
                break;
            case AnalysisResultInterface::TYPE_PRE_FLIGHT_REQUEST:
                $cors_headers = $cors->getResponseHeaders();
                foreach ($cors_headers as $header => $value) {
                    /* Diactoros errors on integer values. */
                    if (false === is_array($value)) {
                        $value = (string)$value;
                    }
                    $response = $response->withAddedHeader($header, $value);
                }
                return $response->withStatus(200);
            case AnalysisResultInterface::TYPE_REQUEST_OUT_OF_CORS_SCOPE:
                return $next($request, $response);
            default:
                /* Actual CORS request. */
                $response = $next($request, $response);
                $cors_headers = $cors->getResponseHeaders();
                foreach ($cors_headers as $header => $value) {
                    /* Diactoros errors on integer values. */
                    if (false === is_array($value)) {
                        $value = (string)$value;
                    }
                    $response = $response->withAddedHeader($header, $value);
                }

                return $response;
        }

        // if we are still here then we got some errors above;
        $exception = new BadRequestException($message);
        $response = $exception->buildJsonResponse($response);
        return $response;
    }

    public function setSettings(SettingsStrategyInterface $settings)
    {
        $this->settings = $settings;
    }

    public function getSettings()
    {
        if (!$this->settings || !$this->settings instanceof SettingsStrategyInterface) {
            $this->settings = new CorsMiddlewareStrategySettings();
        }

        return $this->settings;
    }
}
