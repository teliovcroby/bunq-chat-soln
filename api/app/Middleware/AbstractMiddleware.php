<?php

namespace App\Middleware;

use App\Contracts\ContainerContract;
use Illuminate\Contracts\Container\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

abstract class AbstractMiddleware
{
    use ContainerContract;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    abstract public function __invoke(Request $request, Response $response, callable $next);
}
