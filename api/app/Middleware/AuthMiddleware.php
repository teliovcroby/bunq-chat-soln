<?php


namespace App\Middleware;

use App\Exceptions\AuthenticationException;
use App\Exceptions\PermissionException;
use App\Services\AuthService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthMiddleware extends AbstractMiddleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $header = $request->getHeader('Authorization')[0] ?? null;

        if (!$header) {
            throw new PermissionException();
        }

        $matches = [];
        preg_match("/^Bearer\s+(.*)/", $header, $matches);

        if (empty($matches) || count($matches) != 2) {
            throw new PermissionException();
        }

        $token = $matches[1];

        $authService = $this->getContainer()->make(AuthService::class);

        try {
            $user = $authService->parseToken($token);
        } catch (\Exception $e) {
            throw  new PermissionException($e->getMessage());
        }

        $request = $request->withAttribute('auth_user', $user)
            ->withAttribute('auth_token', $token);

        return $next($request, $response);
    }
}