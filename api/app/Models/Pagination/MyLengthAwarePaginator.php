<?php

namespace App\Models\Pagination;

use Illuminate\Pagination\LengthAwarePaginator;

class MyLengthAwarePaginator extends LengthAwarePaginator
{
    public function toArray()
    {
        return [
            'data' => $this->items->toArray(),
            'pagination' => [
                'total' => $this->total(),
                'current_page' => $this->currentPage(),
                'per_page' => $this->perPage(),
                'last_page' => $this->lastPage(),
                'next_page' => $this->hasMorePages() ? $this->currentPage() + 1 : null
            ]
        ];
    }
}
