<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

trait Extras
{
    public function getDateFormat()
    {
        return "Y-m-d H:i:s";
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (!is_null($keyName)) {
            if (isset($this->original[$keyName])) {
                return $this->original[$keyName];
            }
        }

        return parent::getKeyForSaveQuery();
    }

    public static function getTableName()
    {
        return (new static())->getTable();
    }

    public function fireCustomEvent($event, $payload, $halt = true)
    {
        if (! isset(static::$dispatcher)) {
            return true;
        }

        $event = "eloquent.{$event}: ".static::class;
        $method = $halt ? 'until' : 'fire';

        if (!is_array($payload)) {
            $payload = [$payload];
        }

        array_unshift($payload, $this);

        return static::$dispatcher->$method($event, $payload);
    }

    public function setUpdatedAt($value) {
        if (static::UPDATED_AT) {
            $this->{static::UPDATED_AT} = $value;
        }

        return $this;
    }
}
