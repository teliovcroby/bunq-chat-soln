<?php


namespace App\Models\Mysql;


class Chat extends Base
{
    protected $table = "chats";

    const MORPH_NAME = "chat";
}