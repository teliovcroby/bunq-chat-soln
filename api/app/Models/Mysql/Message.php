<?php


namespace App\Models\Mysql;


class Message extends Base
{
    protected $table = "messages";

    const MORPH_NAME = "message";

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }
}