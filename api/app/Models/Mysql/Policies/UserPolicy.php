<?php

namespace App\Models\Mysql\Policies;

use App\Access\Policies\BasePolicy;
use App\Models\Mysql\User;

class UserPolicy extends BasePolicy
{
    protected function defaultPolicy($resourceObj, $actorObj)
    {
        if ($resourceObj instanceof User === false || $actorObj instanceof User === false) {
            return false;
        }

        if ($resourceObj->id !== $actorObj->id) {
            return false;
        }

        return true;
    }

    public function view($resourceObj, $actorObj)
    {
        return $this->defaultPolicy($resourceObj, $actorObj);
    }

    public function update($resourceObj, $actorObj)
    {
        return $this->defaultPolicy($resourceObj, $actorObj);
    }

    public function create($resourceObj, $actorObj)
    {
        return $this->defaultPolicy($resourceObj, $actorObj);
    }

    public function delete($resourceObj, $actorObj)
    {
        return $this->defaultPolicy($resourceObj, $actorObj);
    }
}
