<?php


namespace App\Models\Mysql;


class User extends Base
{
    protected $table = "users";

    const MORPH_NAME = "user";

    protected $hidden = [
        'password', 'soft_deleted_at'
    ];
}