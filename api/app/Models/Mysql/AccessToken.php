<?php


namespace App\Models\Mysql;


class AccessToken extends Base
{
    protected $table = "access_tokens";

    const MORPH_NAME = "access_token";
}