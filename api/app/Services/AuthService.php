<?php


namespace App\Services;

use App\Contracts\UserManagerTrait;
use App\Exceptions\AuthenticationException;
use App\Exceptions\ExceptionMessages;
use App\Exceptions\ResourceExistsException;
use App\Exceptions\ResourceNotFoundException;
use App\Models\Mysql\User;
use Carbon\Carbon;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\ValidationData;
use Ramsey\Uuid\Uuid;

class AuthService
{
    use UserManagerTrait;
    use ExceptionMessages;

    const TOKEN_LIFE = 86400;

    const TOKEN_ISSUER = 'bunq-chat-api';

    public static function verifyPassword($password, $passwordHash)
    {
        return password_verify($password, $passwordHash);
    }

    public static function getPasswordHash($password)
    {
        return password_hash(
            $password,
            PASSWORD_DEFAULT
        );
    }

    public static function getSigningKey()
    {
        return getenv('APP_SECRET');
    }

    public function generateJwtForToken($token, $userId, $expiresAt)
    {
        $now = Carbon::now();

        $signer = new Sha256();
        $key = new Key(self::getSigningKey());

        $token = (new Builder())
            ->issuedBy(self::TOKEN_ISSUER, true)
            ->identifiedBy($token, true)
            ->issuedAt($now->timestamp)
            ->expiresAt($expiresAt)
            ->withClaim('uid', $userId)
            ->getToken($signer, $key);

        return $token;
    }

    /**
     * @param $userId
     * @return \Lcobucci\JWT\Token
     * @throws \Exception
     */
    public function generateToken($userId)
    {

        $expiresAt = Carbon::now()->addSeconds(self::TOKEN_LIFE);
        $token = Uuid::uuid4()->toString();

        $accessToken = $this->createUserAccessToken($userId, $token, $expiresAt->format("Y-m-d H:i:s"));

        return $this->generateJwtForToken($token, $accessToken->user_id, $expiresAt->timestamp);
    }

    /**
     * @param $username
     * @param $password
     * @return array
     * @throws ResourceExistsException
     */
    public function handleRegistration($username, $password)
    {
        $userExists = $this->userExistsByUsername($username);

        if ($userExists) {
            throw new ResourceExistsException(static::$USER_EXISTS);
        }

        // create user and generate token
        $passwordHash = self::getPasswordHash($password);
        $user = $this->createUser($username, $passwordHash);

        $token = $this->generateToken($user->id);

        return [
            'user' => $user,
            'token_data' => [
                'token' => (string) $token,
                'expires_at' => $token->getClaim('exp')
            ]
        ];
    }

    public function authenticateUser($username, $password)
    {
        $user = $this->getUserByUsername($username);

        if (!$user) {
            throw new ResourceNotFoundException(static::$USER_NOT_FOUND);
        }

        $passwordOk = self::verifyPassword($password, $user->password);

        if (!$passwordOk) {
            throw new AuthenticationException(self::$INVALID_AUTHENTICATION_DETAILS);
        }

        $token = $this->generateToken($user->id);

        return [
            'user' => $user,
            'token_data' => [
                'token' => (string )$token,
                'expires_at' => $token->getClaim('exp')
            ]
        ];
    }

    /**
     * @param $tokenString
     * @return User|null
     * @throws AuthenticationException
     * @throws ResourceNotFoundException
     */
    public function parseToken($tokenString)
    {

        $token = (new Parser())->parse($tokenString);
        // verify the token
        $signer = new Sha256();

        if (!$token->verify($signer, self::getSigningKey())) {
            throw new AuthenticationException("Could not verify token");
        }

        // validate the data
        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer(self::TOKEN_ISSUER);

        if (!$token->validate($data)) {
            throw new AuthenticationException("Could not validate token");
        }

        $tokenId = $token->getClaim('jti', null);
        $userId = $token->getClaim('uid', null);

        if (!$userId || !$tokenId) {
            throw new AuthenticationException("Could not obtain valid user from token");
        }

        // check that token has not been revoked i.e deleted
        $tokenExists = $this->getUserAccessToken($tokenId, $userId);

        if (!$tokenExists) {
            throw new AuthenticationException("Access token has been revoked");
        }

        $user = $this->getUserById($userId);

        if (!$user) {
            throw new ResourceNotFoundException(static::$USER_NOT_FOUND);
        }

        return $user;
    }

    /**
     * @param $tokenString
     * @return bool
     */
    public function revokeToken($tokenString)
    {
        try {
            $token = (new Parser())->parse($tokenString);
            $tokenId = $token->getHeader('jti', null);

            if ($tokenId) {
                $accessToken = $this->getUserAccessToken($tokenId);

                if ($accessToken) {
                    $accessToken->delete();
                }
            }

        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}