<?php


namespace App\Services;


use App\Contracts\PaginationAwareContract;
use App\Contracts\UserManagerTrait;
use App\Models\Mysql\User;

class UserManager
{
    use PaginationAwareContract;

    use UserManagerTrait;

    public function listUsers($perPage=100, $page=1, $query=null)
    {

        $builder = (new User())->newQuery();
        if ($query) {
            $builder = $builder->where('username', 'like', '%'.$query.'%');
        }

        return $this->_paginate($builder, $page, $perPage);
    }
}