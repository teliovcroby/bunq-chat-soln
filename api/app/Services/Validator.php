<?php

namespace App\Services;

use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;
use Valitron\Validator as BaseValidator;

class Validator extends BaseValidator
{
    public function __construct(array $data, array $fields = [], $lang = null, $langDir = null)
    {
        parent::__construct($data, $fields, $lang, $langDir);

        static::$_ruleMessages['mustOnlyContain'] = "Some parameters are included in this request which are not supported,
         check the documentation and ensure only supported fields are included in the request";

        static::$_ruleMessages['arrayMaxLength'] = " is longer than the allowed number of numbers that can be processed at a time";

        static::$_ruleMessages['paramGreaterThan'] = " has an incorrect value.";

        static::$_ruleMessages['password'] = " must be greater than 6 characters and less than 100 characters";

        static::$_ruleMessages['mustExistTogether'] = " must be accompanied by certain other parameters";

        static::$_ruleMessages["lengthMin"] = "must contain greater than or equal to %d characters";

        static::$_ruleMessages["lengthMax"] = "must contain less than or equal to %d characters";
    }

    protected function validateMustOnlyContain($field, $value)
    {
        $param = func_get_args()[2][0];
        return count(array_diff(array_keys($this->_fields), $param)) > 0 ? false : true;
    }


    protected function validatePassword($field, $value)
    {
        if (!is_string($value)) {
            return false;
        }

        $passwordLength = strlen($value);
        if ($passwordLength < 6 || $passwordLength > 100) {
            return false;
        }

        return true;
    }

    protected function validateArrayMaxLength($field, $value)
    {
        if (!is_array($value)) return false;

        $param = func_get_args()[2][0];

        if (count($value) > intval($param)) {
            return false;
        }

        return true;
    }

    protected function validateParamGreaterThan($field, $value, $params)
    {
        list($compare,$_) = $this->getPart($this->_fields, explode(".", $params[0]));
        return $value > $compare;
    }

    protected function validateMustExistTogether($field, $value, $params)
    {
        $partners = $params[0];

        if (!is_array($partners))  {
            $partners = [$partners];
        }

        foreach ($partners as $partner) {
            list($compare, $_) = $this->getPart($this->_fields, explode(".", $partner));
            if (!$compare) {
                return false;
            }
        }

        return true;
    }

    /**
     * Run validations and return boolean result
     *
     * @return boolean
     */
    public function validate()
    {
        foreach ($this->_validations as $v) {
            foreach ($v['fields'] as $field) {
                list($values, $multiple) = $this->getPart($this->_fields, explode('.', $field));

                // Don't validate if the field is not required and the value is empty
                if ($field !== "**all**" && $v['rule'] !== 'required' && !$this->hasRule('required', $field)
                    && (! isset($values) || $values === '' || ($multiple && count($values) == 0))) {
                    continue;
                }

                // Callback is user-specified or assumed method on class
                if (isset(static::$_rules[$v['rule']])) {
                    $callback = static::$_rules[$v['rule']];
                } else {
                    $callback = array($this, 'validate' . ucfirst($v['rule']));
                }

                if (!$multiple) {
                    $values = array($values);
                }

                $result = true;
                foreach ($values as $value) {
                    $result = $result && call_user_func($callback, $field, $value, $v['params']);
                }

                if (!$result) {
                    $this->error($field, $v['message'], $v['params']);
                }
            }
        }

        return count($this->errors()) === 0;
    }

    public function errorsString()
    {
        $errors = $this->errors();
        $string = "";

        foreach ($errors as $error => $info) {
            $string .= $error.":\n";
            foreach ($info as $item) {
                if (is_array($item)) {
                    $item = implode($item);
                }
                $string .= "\t".$item;
            }
            $string .= "\n";
        }
        return $string;
    }
}
