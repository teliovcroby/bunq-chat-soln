<?php


namespace App\Services;


use App\Contracts\PaginationAwareContract;
use App\Contracts\UserManagerTrait;
use App\Exceptions\ExceptionMessages;
use App\Exceptions\ResourceNotFoundException;
use App\Models\Mysql\Message;
use App\Models\Mysql\User;

class MessageService
{
    use ExceptionMessages;

    use PaginationAwareContract;

    use UserManagerTrait;

    /**
     * @param $senderId
     * @param $recipientId
     * @param $text
     * @return Message
     * @throws ResourceNotFoundException
     */
    public function postMessage($sender, $recipientId, $text)
    {
        $recipientExists = $this->userExistsById($recipientId);

        if (!$recipientExists) {
            throw new ResourceNotFoundException(static::$RECIPIENT_NOT_FOUND);
        }

        $message = $this->saveUserMessage($sender->id, $recipientId, $text);

        return $message;
    }

    public function listUserMessages($userId, $queryParams = [])
    {
        $otherUsername = $queryParams['username'] ?? null;
        $perPage = $queryParams['per_page'] ?? 100;
        $page = $queryParams['page'] ?? 1;
        $after = $queryParams['after'] ?? null;
        $before = $queryParams['before'] ?? null;

        $builder = (new Message())->newQuery();

        if ($after && $before) {
            $builder = $builder->whereBetween('created_at', [$after, $before]);
        } elseif ($after) {
            $builder = $builder->where('created_at', '>=', $after);
        } elseif ($before) {
            $builder = $builder->where('created_at', '<=', $before);
        }

        if (!$otherUsername) {
            $builder = $builder->where('sender_id', $userId)
                ->orWhere('receiver_id', $userId)
                ->with(['sender', 'receiver']);
        } else {
            $otherUser = (new User())->where('username', $otherUsername)->first();
            if (!$otherUser) {
                throw new ResourceNotFoundException(static::$OTHER_USER_NOT_FOUND);
            }
            $otherUserId = $otherUser->id;

            $builder = $builder
                ->where(function ($query) use ($userId, $otherUserId) {
                    $query->where('sender_id', $userId)
                        ->where('receiver_id', $otherUserId);
                })
                ->orWhere(function ($query) use ($userId, $otherUserId){
                    $query->where('sender_id', $otherUserId)
                        ->where('receiver_id', $userId);
                });
        }

        return $this->_paginate($builder, $page, $perPage);
    }

    public function listUserChats($userId, $queryParams = [])
    {
        $sql = "select id, username from users where id in (
                select distinct case
                    when m.receiver_id=? then m.sender_id
                    when m.sender_id=? then m.receiver_id
                end as uniqe_id
                from messages m
                )";

        $pdo = (new Message())->getConnection()->getPdo();

        $stmt = $pdo->prepare($sql);

        $stmt->execute([$userId, $userId]);

        $activeChats = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $activeChats;
    }
}