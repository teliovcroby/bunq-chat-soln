<?php

namespace App\Contracts;

use Illuminate\Contracts\Container\Container;

trait ContainerContract
{
    /**
     * @var Container
     */
    public $container;

    public function getContainer()
    {
        if (!$this->container) {
            $this->container = app();
        }

        return $this->container;
    }

    public function setContainer(Container $container)
    {
        $this->container = $container;
        return $this;
    }
}
