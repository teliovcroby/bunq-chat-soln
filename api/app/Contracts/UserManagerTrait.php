<?php


namespace App\Contracts;


use App\Models\Mysql\AccessToken;
use App\Models\Mysql\Message;
use App\Models\Mysql\User;

trait UserManagerTrait
{
    public function getUserById($userId)
    {
        return (new User())->find($userId);
    }

    public function getUserByUsername($username)
    {
        return (new User())->where('username', $username)->first();
    }

    public function userExistsById($userId)
    {
        return (new User())->where('id', $userId)->exists();
    }

    public function userExistsByUsername($username)
    {
        return (new User())->where('username', $username)->exists();
    }

    public function createUser($username, $passwordHash)
    {
        $user = new User();
        $user->username = $username;
        $user->password = $passwordHash;
        $user->save();

        return $user;
    }

    public function getUserAccessToken($tokenId, $userId = null)
    {
        $builder = (new AccessToken())
            ->where('token', $tokenId);

        if ($userId) {
            $builder = $builder->where('user_id', $userId);
        }

        return $builder->first();
    }

    public function createUserAccessToken($userId, $token, $expiresAt)
    {
        $accessToken = new AccessToken();
        $accessToken->user_id = $userId;
        $accessToken->token = $token;
        $accessToken->expires_at = $expiresAt;

        $accessToken->save();

        return $accessToken;
    }

    public function saveUserMessage($userId, $recipientId, $text)
    {
        $message = new Message();
        $message->message = $text;
        $message->sender_id = $userId;
        $message->receiver_id = $recipientId;
        $message->save();

        return $message;
    }
}