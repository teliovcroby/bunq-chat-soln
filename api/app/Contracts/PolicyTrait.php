<?php

namespace App\Contracts;

use App\Access\Policies\PolicyManager;

trait PolicyTrait
{
    /**
     * @var PolicyManager
     */
    protected $policyManager;

    /**
     * @return PolicyManager
     */
    public function getPolicyManager()
    {
        if (!$this->policyManager) {
            $this->policyManager = $this->getContainer()->make(PolicyManager::class);
        }

        return $this->policyManager;
    }

    /**
     * @param $policyManager PolicyManager
     * @return $this
     */
    public function setPolicyManager(PolicyManager $policyManager)
    {
        $this->policyManager = $policyManager;
        return $this;
    }
}
