<?php

namespace App\Contracts;

use App\Services\Validator;

trait ValidatorTrait
{
    public function validate($rules, $data)
    {
        $validator = new Validator($data);

        $validator->rules($rules);

        $isValid = $validator->validate();

        if (!$isValid) {
            return [
                'status' => $isValid,
                'error' => $validator->errorsString()
            ];
        }

        return [
            'status' => $isValid,
            'data' => $data
        ];
    }
}