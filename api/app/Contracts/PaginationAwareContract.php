<?php

namespace App\Contracts;

use App\Models\Pagination\MyLengthAwarePaginator;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Support\Collection;
use Psr\Http\Message\ServerRequestInterface as Request;

trait PaginationAwareContract
{
    /**
     * @param $builder
     * @throws \Exception
     */
    public function _validateBuilder($builder)
    {
        if ($builder instanceof Builder === false && $builder instanceof EloquentBuilder === false && $builder instanceof Relation === false) {
            $msg = sprintf(
                "Builder instance passed to paginate must either be of type %s or type %s",
                Builder::class,
                EloquentBuilder::class
            );
            throw new \Exception($msg);
        }
    }

    /**
     * @param $builder
     * @param int $currentPage
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function _paginate($builder, $currentPage = 1, $perPage = 1000, $columns = ['*'])
    {
        $total = $builder instanceof Builder ? $builder->getCountForPagination($columns) :
            $builder->getQuery()->getCountForPagination($columns);

        $items = $total ? $builder->forPage($currentPage, $perPage)
            ->get($columns) : new Collection();

        $options = [];
        return Container::getInstance()
            ->makeWith(MyLengthAwarePaginator::class, compact(
                'items', 'total', 'perPage', 'currentPage', 'options'
            ));
    }

    /**
     * @param $builder
     * @param Request $request
     * @param array $columns
     * @param string $pageName
     * @param string $perPageName
     * @return mixed
     * @throws \Exception
     */
    public function paginate($builder, Request $request, $columns = ['*'], $pageName = 'page', $perPageName = 'per_page')
    {
        $this->_validateBuilder($builder);

        $queryParams = $request->getQueryParams();


        $currentPage = $queryParams[$pageName] ?? 1;
        $perPage = $queryParams[$perPageName] ?? 1000;

        return $this->_paginate($builder, $currentPage, $perPage, $columns);
    }
}
