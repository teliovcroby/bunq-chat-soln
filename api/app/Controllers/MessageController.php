<?php


namespace App\Controllers;

use App\Access\Policies\PolicyException;
use App\Contracts\PolicyTrait;
use App\Exceptions\BadRequestException;
use App\Exceptions\PermissionException;
use App\Exceptions\ResourceNotFoundException;
use App\Models\Mysql\User;
use App\Services\MessageService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class MessageController extends BaseController
{
    use PolicyTrait;

    /**
     * @param Request $request
     * @param Response $response
     * @param MessageService $messageService
     * @return Response
     * @throws BadRequestException
     * @throws PermissionException
     * @throws ResourceNotFoundException
     * @throws PolicyException
     */
    public function postChatMessage(
        Request $request,
        Response $response,
        MessageService $messageService
    ) {
        $validationRules = [
            "required" => [
                ["sender_id"],
                ["receiver_id"],
                ["message"]
            ],

            "mustOnlyContain" => [
                [
                    '**all**', ['sender_id', 'receiver_id', 'message']
                ]
            ]
        ];

        $requestData =  $this->validate($request, $validationRules);

        $senderId = $requestData['sender_id'];
        $receiverId = $requestData['receiver_id'];
        $text = $requestData['message'];

        $sender = (new User())->find($senderId);
        if (!$sender) {
            throw new ResourceNotFoundException(static::$SENDER_NOT_FOUND);
        }

        $authUser = $request->getAttribute('auth_user');
        $hasPermission = $this->getPolicyManager()
            ->executePolicy(
                User::class.".view",
                $sender,
                $authUser
            );

        if (!$hasPermission) {
            throw new PermissionException();
        }

        $message = $messageService->postMessage($sender, $receiverId, $text);

        return $this->addDataToResponse($response, $message);
    }
}