<?php


namespace App\Controllers;

use App\Contracts\PolicyTrait;
use App\Exceptions\PermissionException;
use App\Exceptions\ResourceNotFoundException;
use App\Models\Mysql\User;
use App\Services\MessageService;
use App\Services\UserManager;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class UserController extends BaseController
{
    use PolicyTrait;

    public function listUsers(Request $request, Response $response, UserManager $manager)
    {
        $queryParams = $request->getQueryParams();

        $perPage = $queryParams['per_page'] ?? 100;
        $page = $queryParams['page'] ?? 1;
        $username = $queryParams['username'] ?? null;

        $data = $manager->listUsers($perPage, $page, $username);
        return $this->addToResponse($response, $data);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $userId
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getUserProfile(Request $request, Response $response, $userId)
    {
        $user = (new User())->where('id', $userId)->first();

        if (!$user) {
            throw new ResourceNotFoundException(static::$USER_NOT_FOUND);
        }

        return $this->addDataToResponse($response, $user);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $userId
     * @param MessageService $messageService
     * @return Response
     * @throws PermissionException
     * @throws ResourceNotFoundException
     * @throws \App\Access\Policies\PolicyException
     */
    public function listUserMessages(
        Request $request,
        Response $response,
        $userId,
        MessageService $messageService
    ) {
        $user = (new User())->where('id', $userId)->first();

        if (!$user) {
            throw new ResourceNotFoundException(static::$USER_NOT_FOUND);
        }

        $authUser = $request->getAttribute('auth_user');
        $hasPermission = $this->getPolicyManager()
            ->executePolicy(
                User::class.".view",
                $user,
                $authUser
            );

        if (!$hasPermission) {
            throw new PermissionException();
        }

        $queryParams = $request->getQueryParams();
        $messages = $messageService->listUserMessages($user->id, $queryParams);

        return $this->addDataToResponse($response, $messages);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $userId
     * @param MessageService $messageService
     * @return Response
     * @throws PermissionException
     * @throws ResourceNotFoundException
     * @throws \App\Access\Policies\PolicyException
     */
    public function listUserChats(Request $request, Response $response, $userId, MessageService $messageService)
    {
        $user = (new User())->where('id', $userId)->first();

        if (!$user) {
            throw new ResourceNotFoundException(static::$USER_NOT_FOUND);
        }

        $authUser = $request->getAttribute('auth_user');
        $hasPermission = $this->getPolicyManager()
            ->executePolicy(
                User::class.".view",
                $user,
                $authUser
            );

        if (!$hasPermission) {
            throw new PermissionException();
        }

        $queryParams = $request->getQueryParams();

        $data = $messageService->listUserChats($userId, $queryParams);
        return $this->addToResponse($response, $data);
    }
}