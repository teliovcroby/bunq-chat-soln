<?php

namespace App\Controllers;

use App\Contracts\ContainerContract;
use App\Contracts\PaginationAwareContract;
use App\Contracts\PolicyTrait;
use App\Exceptions\ExceptionMessages;
use App\Exceptions\BadRequestException;
use App\Models\Pagination\MyLengthAwarePaginator;
use App\Services\Validator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class BaseController
{
    use ExceptionMessages;
    use PaginationAwareContract;
    use PolicyTrait;
    use ContainerContract;

    /**
     * @param $input
     * @param $key
     */
    public function escapeString(&$input, $key)
    {
        $allowHTML = false;
        $input = trim($input);

        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }

        # Normalize newlines
        $input = str_replace("\r", "\n", $input);
        $input = preg_replace("/\n\n+/", "\n", $input);

        # Escape HTML
        $input = htmlentities($input, ENT_QUOTES, 'UTF-8');

        $input = nl2br($input);
        if ($allowHTML) {
            # em
            $input = preg_replace('!&lt;em&gt;(.*?)&lt;/em&gt;!im', '<em>$1</em>', $input);

            # i
            $input = preg_replace('!&lt;i&gt;(.*?)&lt;/i&gt;!im', '<i>$1</i>', $input);

            # b
            $input = preg_replace('!&lt;b&gt;(.*?)&lt;/b&gt;!im', '<b>$1</b>', $input);

            # u
            $input = preg_replace('!&lt;u&gt;(.*?)&lt;/u&gt;!im', '<u>$1</u>', $input);

            # a
            $input = preg_replace("!&lt;a +href=&quot;((?:ht|f)tps?://.*?)&quot;(?: +title=&quot;(.*?)&quot;)?(?: +rel=&quot;(.*?)&quot;)? *&gt;(.*?)&lt;/a&gt;!im", '<a href="$1">$4</a>', $input);
            $input = preg_replace("!&lt;a +href=&quot;(#.*?)&quot; *&gt;(.*?)&lt;/a&gt;!im", '<a href="$1">$2</a>', $input);
            $input = preg_replace("'(?<!=\")(http|ftp)://([\w\+\-\@\=\?\.\%\/\:\&\;~\|]+)(\.)?'im", "<a href=\"\\1://\\2\">\\1://\\2</a>", $input);
        }

    }

    protected function getRequestBody(Request $request)
    {
        return json_decode((string) $request->getBody(), true) ?: [];
    }

    protected function validate(Request $request, array $rules, $isQuery = false, $includePost = false)
    {
        if (!$isQuery) {
            $requestBody = $this->getRequestBody($request);

            if ($includePost) {
                $requestBody = array_merge($requestBody, $_POST);
            }
        } else {
            $requestBody = $request->getQueryParams();
        }

        $validator = new Validator($requestBody);
        $validator->rules($rules);

        if (!$validator->validate()) {
            throw new BadRequestException($validator->errorsString());
        }

        array_walk_recursive($requestBody, [$this, 'escapeString']);

        return $requestBody;
    }

    protected function extractPaginationFromRequest(Request $request)
    {
        $queryParams = $request->getQueryParams();

        $perPage = isset($queryParams["per_page"]) ? $queryParams["per_page"] : 1000;
        $page = isset($queryParams["page"]) ? $queryParams["page"] : 1;

        return compact("perPage", "page");
    }

    protected function addDataToResponse(Response $response, $data)
    {
        if ($data instanceof MyLengthAwarePaginator) {
            $data = $data->toArray();
        } else {
            $data = json_decode(json_encode($data), true);

            if (!is_array($data) || !isset($data['data'])) {
                $data = [
                    'data' => $data
                ];
            }
        }


        $response->getBody()->write(json_encode($data));
        $response = $response->withAddedHeader('content-type', 'application/json');
        return $response;
    }

    protected function addToResponse(Response $response, $data)
    {
        $response->getBody()->write(json_encode($data));
        $response = $response->withAddedHeader('content-type', 'application/json');
        return $response;
    }
}
