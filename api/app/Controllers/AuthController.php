<?php


namespace App\Controllers;

use App\Exceptions\AuthenticationException;
use App\Exceptions\BadRequestException;
use App\Exceptions\ResourceExistsException;
use App\Exceptions\ResourceNotFoundException;
use App\Services\AuthService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AuthController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param AuthService $authService
     * @return Response
     * @throws AuthenticationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     */
    public function getAccessTokenForUser(
        Request $request,
        Response $response,
        AuthService $authService
    )
    {
        $validationRules = [
            "required" => [
                ["username"],
                ["password"]
            ],

            "mustOnlyContain" => [
                [
                    '**all**', ['username', 'password']
                ]
            ]
        ];

        $requestData = $this->validate($request, $validationRules);

        $username = $requestData['username'];
        $password = $requestData['password'];

        $authUser = $authService->authenticateUser($username, $password);
        return $this->addDataToResponse($response, $authUser);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function whoAmI(Request $request, Response $response)
    {
        return $this->addDataToResponse($response, $request->getAttribute('auth_user', null));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param AuthService $authService
     * @return Response
     */
    public function revokeAccessToken(Request $request, Response $response, AuthService $authService)
    {
        $token = $request->getAttribute('auth_token', null);

        $ok = $authService->revokeToken($token);

        return $this->addDataToResponse($response, $ok);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param AuthService $authService
     * @return Response
     * @throws BadRequestException
     * @throws ResourceExistsException
     */
    public function handleRegistration(
        Request $request,
        Response $response,
        AuthService $authService
    ){
        $validationRules = [
            "required" => [
                ["username"],
                ["password"]
            ],

            "mustOnlyContain" => [
                [
                    '**all**', ['username', 'password']
                ]
            ]
        ];

        $requestData = $this->validate($request, $validationRules);

        $username = $requestData['username'];
        $password = $requestData['password'];

        $newReg = $authService->handleRegistration($username, $password);
        return $this->addDataToResponse($response, $newReg);
    }
}