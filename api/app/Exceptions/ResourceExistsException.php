<?php

namespace App\Exceptions;

class ResourceExistsException extends BaseException
{
    protected static $httpStatusCode = 400;

    protected static $errorCode = "E110";

    protected static $errorMessage = "This resource was not found";
}
