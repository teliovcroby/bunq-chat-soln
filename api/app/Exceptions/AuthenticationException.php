<?php

namespace App\Exceptions;

class AuthenticationException extends BaseException
{
    protected static $httpStatusCode = 401;

    protected static $errorCode = "E107";

    protected static $errorMessage = "There was a problem authenticating this request";
}
