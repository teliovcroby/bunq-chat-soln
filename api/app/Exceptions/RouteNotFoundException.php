<?php

namespace App\Exceptions;

class RouteNotFoundException extends BaseException
{
    protected static $httpStatusCode = 404;

    protected static $errorCode = "E103";

    protected static $errorMessage = "This route does not exist";
}
