<?php


namespace App\Exceptions;

trait ExceptionMessages
{
    public static $USER_NOT_FOUND = "The specified user was not found";

    public static $OTHER_USER_NOT_FOUND = "The specified filter user was not found";

    public static $SENDER_NOT_FOUND = "The specified sender was not found";

    public static $RECIPIENT_NOT_FOUND = "The specified recipient was not found";

    public static $INVALID_AUTHENTICATION_DETAILS = "Invalid Authentication Parameters";

    public static $USER_EXISTS = "User already exists";


}
