<?php

namespace App\Exceptions;

use JsonSerializable;

class BaseException extends \Exception implements JsonSerializable
{
    protected static $errorMessage;

    protected static $errorCode;

    protected static $httpStatusCode;

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, 0, $previous);


        if ($message) {
            static::$errorMessage = $message;
        } elseif ($previous) {
            static::$errorMessage = $previous->getMessage();
        }
    }

    public function toArray()
    {
        return [
            "error" => static::$errorMessage,
            "error_code" => static::$errorCode,
            "http_status_code" => static::$httpStatusCode
        ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
