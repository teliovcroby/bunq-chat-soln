<?php

namespace App\Exceptions;

class MethodNotAllowedException extends BaseException
{
    protected static $httpStatusCode = 405;

    protected static $errorCode = "E104";

    protected static $errorMessage = "This request method is not allowed for the specified uri";
}
