<?php

namespace App\Exceptions;

class BadRequestException extends BaseException
{
    protected static $httpStatusCode = 400;

    protected static $errorCode = "E102";

    protected static $errorMessage = "The request body contains invalid data";
}
