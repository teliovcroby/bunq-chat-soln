<?php

namespace App\Exceptions;

class ServerException extends BaseException
{
    protected static $httpStatusCode = 500;

    protected static $errorCode = "E105";
}
