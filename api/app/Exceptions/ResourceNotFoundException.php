<?php

namespace App\Exceptions;

class ResourceNotFoundException extends BaseException
{
    protected static $httpStatusCode = 404;

    protected static $errorCode = "E106";

    protected static $errorMessage = "This resource was not found";
}
