<?php

namespace App\Exceptions;

class PermissionException extends BaseException
{
    protected static $httpStatusCode = 403;

    protected static $errorCode = "E109";

    protected static $errorMessage = "You do not have permission to perform this action";
}
