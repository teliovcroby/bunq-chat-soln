<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use Illuminate\Events\Dispatcher;

class EventServiceProvider extends AbstractProvider
{
    public function register()
    {
        $this->container->singleton(Dispatcher::class, function (Container $container) {
            return new Dispatcher($container);
        });

        $this->container->alias(Dispatcher::class, "app.events");

        $this->registerSubscribers();
    }

    public function registerSubscribers()
    {

        $subscribers = app_config_dir()."/listeners";
        if ($subscribers && is_array($subscribers) && !empty($subscribers)) {
            $dispatcher = $this->getContainer()->make('app.events');
            foreach ($subscribers as $subscriber) {
                $dispatcher->subscribe($subscriber);
            }
        }
    }
}
