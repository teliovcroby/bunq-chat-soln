<?php

namespace App\Providers;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Relations\Relation;

class DatabaseProvider extends AbstractProvider
{
    public function register()
    {
        // These variables must have been set in the .env file
        $dbName = getenv("DB_NAME");

        $dbFile = app_base_dir()."/".$dbName.".sqlite";
        if (!file_exists($dbFile)) {
            touch($dbFile);
        }

        $manager = new Manager($this->container);

        // Add the mysql connection
        // This is made the default connection (in the case where mongodb is being used also)
        $manager->addConnection([
            'driver' => "sqlite",
            'database' => $dbFile,
            'prefix'    => '',
        ], 'default');
        $manager->getDatabaseManager()->setDefaultConnection('default');

        // register relations

        $morphMaps = app_config_dir()."/morphmaps.php";
        if ($morphMaps && is_array($morphMaps)) {
            Relation::morphMap($morphMaps);
        }

        // make this manager instance available globally
        // boot Eloquent
        $manager->setAsGlobal();
        $dispatcher = $this->container->make('app.events');
        $manager->setEventDispatcher($dispatcher);
        $manager->bootEloquent();

        $this->container->singleton(Manager::class, function () use ($manager) {
            return $manager;
        });

        $this->container->singleton(\PDO::class, function () use ($manager) {
            return $manager->getConnection()->getPdo();
        });
    }
}
