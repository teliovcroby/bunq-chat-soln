<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use League\Route\RouteCollection;

class RouterServiceProvider extends AbstractProvider
{
    public function register()
    {
        $routeConfig = app_base_dir()."/config/routes.php";
        if (!file_exists($routeConfig)) {
            throw new \Exception("No route config file found");
        }

        $this->container->singleton(RouteCollection::class, function (Container $container) use ($routeConfig) {
            return require_once $routeConfig;
        });

        $this->container->alias(RouteCollection::class, "app.router");
    }
}
