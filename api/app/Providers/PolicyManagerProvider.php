<?php

namespace App\Providers;

use App\Access\Policies\PolicyManager;
use Illuminate\Contracts\Container\Container;

class PolicyManagerProvider extends AbstractProvider
{
    public function register()
    {
        $this->container->singleton(PolicyManager::class, function(Container $container) {
            $manager = new PolicyManager($container);
            $policies = require  app_config_dir()."/policies.php";

            if (is_array($policies)) {
                foreach ($policies as $resource => $executioner) {
                    $manager->registerPolicyForResource($resource, $executioner);
                }
            } else {
                throw new \Exception("Policies must be in array format");
            }

            return $manager;
        });
    }
}
