<?php

namespace App\Providers;

use App\Contracts\ContainerContract;
use Illuminate\Contracts\Container\Container;

abstract class AbstractProvider
{
    use ContainerContract;
    /**
     * @var Container;
     */
    public $container;

    public function __construct(Container $container = null)
    {
        $this->container = $container;
    }

    abstract public function register();
}
