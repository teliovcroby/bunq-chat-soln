<?php

include_once "bootstrap.php";

print(implode('/',[app_base_dir(), getenv('DB_NAME'), ".sqlite"]));

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/data/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/data/seeds'
    ],

    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'development' => [
            'adapter' => 'sqlite',
            'name' => implode('',[app_base_dir(), "/", getenv('DB_NAME'), ".sqlite"]),
            'charset' => 'utf8'
        ],
        'test' => [
            'adapter' => 'sqlite',
            'name' => ':memory:',
            'charset' => 'utf8'
        ]
    ]
];
